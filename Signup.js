import * as React from 'react';
import {View, Text, TouchableOpacity, TextInput, Image} from 'react-native';

const Signup = ({navigation}) => {
  return (
    <View
      style={{
        backgroundColor: '#F4F4F4',
        flex: 1,
        alignItems: 'center',
      }}>
      <Image
        style={{
          marginTop: 150,
          width: 190,
          height: 45,
        }}
        source={require('./Marlin.png')}
      />
      <Text
        style={{
          marginTop: 20,
          fontSize: 18,
          fontWeight: '400',
        }}>
        Create an account
      </Text>
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Name"
      />
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Email"
      />
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Phone"
      />
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Password"
      />
      <TouchableOpacity
        style={{
          marginTop: 20,
          backgroundColor: '#2E3283',
          height: 43,
          width: 273,
          borderRadius: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'white',
            fontWeight: '400',
            fontSize: 12,
          }}>
          Sign up
        </Text>
      </TouchableOpacity>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          height: 20,
          width: '100%',
          borderTopColor: 'lightgray',
          borderTopWidth: 2,
          marginTop: 60,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Text style={{fontSize: 12}}>Already have account</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Signin')}>
          <Text style={{fontSize: 12, fontWeight: 'bold'}}>Log in</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Signup;
