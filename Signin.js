import * as React from 'react';
import {View, Text, TouchableOpacity, TextInput, Image} from 'react-native';

const Signin = ({navigation}) => {
  return (
    <View
      style={{
        backgroundColor: '#F4F4F4',
        flex: 1,
        alignItems: 'center',
      }}>
      <Image
        style={{
          marginTop: 100,
          width: 190,
          height: 45,
        }}
        source={require('./Marlin.png')}
      />
      <Text
        style={{
          marginTop: 20,
          fontSize: 18,
          fontWeight: '400',
        }}>
        Please sign in to continue
      </Text>
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Username"
      />
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Password"
      />
      <TouchableOpacity
        style={{
          marginTop: 20,
          backgroundColor: '#2E3283',
          height: 43,
          width: 273,
          borderRadius: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'white',
            fontWeight: '400',
            fontSize: 12,
          }}>
          Sign in
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Password')}>
        <Text
          style={{
            marginTop: 20,
            color: '#2E3283',
            fontWeight: '400',
            fontSize: 11,
          }}>
          Forgot password
        </Text>
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 30,
          marginHorizontal: 74,
          alignItems: 'center',
        }}>
        <View style={{flex: 1, height: 1, backgroundColor: '#2E3283'}} />
        <Text style={{marginHorizontal: 10, fontSize: 11, fontWeight: '400'}}>
          Login with
        </Text>
        <View style={{flex: 1, height: 1, backgroundColor: '#2E3283'}} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 20,
          alignItems: 'center',
        }}>
        <Image
          style={{marginHorizontal: 20}}
          source={require('./facebook.png')}
        />
        <Image
          style={{marginHorizontal: 20}}
          source={require('./google.png')}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 30,
          alignItems: 'center',
        }}>
        <Text style={{marginHorizontal: 20}}>App version</Text>
        <Text style={{marginHorizontal: 20}}>2.83</Text>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          height: 20,
          width: '100%',
          borderTopColor: 'lightgray',
          borderTopWidth: 2,
          marginTop: 30,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
        }}>
        <Text style={{fontSize: 12}}>Don't have account</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
          <Text style={{fontSize: 12, fontWeight: 'bold'}}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Signin;
