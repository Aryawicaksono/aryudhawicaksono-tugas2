import * as React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Image,
} from 'react-native';

const Password = () => {
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: '#F4F4F4',
        alignItems: 'center',
        paddingTop: 150,
      }}>
      <Image
        style={{
          width: 190,
          height: 45,
        }}
        source={require('./Marlin.png')}
      />
      <Text
        style={{
          marginTop: 20,
          fontSize: 18,
          fontWeight: '400',
        }}>
        Reset your password
      </Text>
      <TextInput
        style={{
          marginTop: 20,
          backgroundColor: '#FFFFFF',
          height: 43,
          width: 273,
          borderRadius: 10,
        }}
        placeholder="        Email"
      />
      <TouchableOpacity
        style={{
          marginTop: 20,
          backgroundColor: '#2E3283',
          height: 43,
          width: 273,
          borderRadius: 10,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            color: 'white',
            fontWeight: '400',
            fontSize: 12,
          }}>
          Request Reset
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default Password;
